using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ABI_RC.Core.Player;
using UnityEngine;
using static MelonLoader.MelonLogger;
using UnityEngine.Events;

namespace VibeGoesBrrr
{
  class ThrustVectorProvider : ISensorProvider, IDisposable
  {
    public class Giver : Sensor
    {
      public static Regex Pattern = new Regex(@"^\s*?(Thrust\s*Vector)\s*(.*)$", RegexOptions.IgnoreCase);

      public float mValue;
      public float mBaseLength;
      public GameObject mMeshObject;
      public Mesh mMesh;

      public float Length {
        get {
          return mBaseLength * mMeshObject.transform.TransformVector(Vector3.forward).magnitude;
        }
      }

      protected GameObject mGameObject;

      public Giver(string name, SensorOwnerType ownerType, GameObject gameObject, GameObject meshObject, Mesh mesh)
        : this(Pattern, name, ownerType, gameObject, meshObject, mesh)
      { }

      public Giver(Regex pattern, string name, SensorOwnerType ownerType, GameObject gameObject, GameObject meshObject, Mesh mesh)
        : base(pattern, name, ownerType)
      {
        mGameObject = gameObject;
        mMeshObject = meshObject;
        mMesh = mesh;
        // Calculate penetrator length based on mesh bounds z
        // We can't use _Length, since it seems to be off by a lot
        mBaseLength = CalculateGiverMeshLength(mMesh);
        // mBaseLength = meshObject.GetComponent<Renderer>().sharedMaterial.GetFloat("_Length"); 
      }

      public override GameObject GameObject => mGameObject;
      public override bool Enabled { get; set; }
      public override float Value => mValue;

      private static float CalculateGiverMeshLength(Mesh mesh)
      {
        float length = 0f;
        var vertices = mesh.vertices;
        foreach (var vertex in vertices) {
          length = Math.Max(length, vertex.z);
        }
        return length;
      }
    }

    public class DPSPenetrator : Giver
    {
      public static float TipID = 0.09f;
      // The Zawoo Hybrid Anthro Peen is set up with a value of .06 by accident
      public static float TipID_ZawooCompat = 0.06f; 

      public Light mLight0;

      public DPSPenetrator(string name, SensorOwnerType ownerType, GameObject gameObject, GameObject meshObject, Mesh mesh, Light light)
        : base(name, ownerType, gameObject, meshObject, mesh)
      {
        mLight0 = light;
      }

      public override bool Active {
        get {
          if (mLight0 != null) {
            return mLight0.gameObject.activeInHierarchy;
          } else {
            return false;
          }
        }
      }
    }

    public class Taker : Sensor
    {
      public static Regex Pattern => Giver.Pattern; // Giver and Taker use the same pattern, since difference is based on mesh presence or DPS identification

      public float mCumulativeValue = 0f;
      public int mNumPenetrators = 0;

      protected GameObject mGameObject;

      public Taker(string name, SensorOwnerType ownerType, GameObject gameObject)
        : this(Pattern, name, ownerType, gameObject)
      { }

      public Taker(Regex pattern, string name, SensorOwnerType ownerType, GameObject gameObject)
        : base(pattern, name, ownerType)
      {
        mGameObject = gameObject;
      }

      public override GameObject GameObject => mGameObject;
      public override bool Enabled { get; set; }

      public override float Value { 
        get {
          // Taker values are an average of all current givers, for that DP action �
          return mNumPenetrators > 0 ? mCumulativeValue / mNumPenetrators : 0f;
        }
      }
    }

    public class DPSOrifice : Taker
    {
      public static float MiddleID = 0.05f;
      public static float EntranceID_A = 0.01f;
      public static float EntranceID_B = 0.02f;

      public Light mLight1 = null;
      public Light mLight2 = null;

      public DPSOrifice(string name, SensorOwnerType ownerType, GameObject gameObject)
        : base(name, ownerType, gameObject) 
      { }

      public override bool Active {
        get {
          if (mLight1 != null && mLight2 != null) {
            return mLight1.gameObject.activeInHierarchy && mLight2.gameObject.activeInHierarchy;
          } else {
            return false;
          }
        }
      }
    }

    public bool Enabled { get; set; }
    public IEnumerable<Sensor> Sensors => mSensorInstances.Values;
    public int Count => mSensorInstances.Count;
    public event EventHandler<Sensor> SensorDiscovered;
    public event EventHandler<Sensor> SensorLost;

    private Dictionary<int, Sensor> mSensorInstances = new Dictionary<int, Sensor>();
    private Dictionary<int, Giver> mGivers = new Dictionary<int, Giver>();
    private Dictionary<int, Taker> mTakers = new Dictionary<int, Taker>();

    public ThrustVectorProvider()
    {
      // m_avatarSetupCompleted += OnAvatarIsReady;
      // PlayerSetup.Instance.avatarSetupCompleted.AddListener(m_avatarSetupCompleted);
      CVRHooks.AvatarIsReady += OnAvatarIsReady;
      CVRHooks.PropIsReady += OnPropIsReady;
    }

    public void Dispose()
    {
      CVRHooks.AvatarIsReady -= OnAvatarIsReady;
      CVRHooks.PropIsReady -= OnPropIsReady;
    }

    public void OnSceneWasInitialized()
    {
      // In CVR, avatars load before the main scene is initialized.
      // We shouldn't need this as long as OnUpdate removes destroyed sensors :)
      // foreach (var kv in mSensorInstances) {
      //   SensorLost?.Invoke(this, kv.Value);
      // }
      // mSensorInstances.Clear();
      // mGivers.Clear();
      // mTakers.Clear();

      foreach (var light in Resources.FindObjectsOfTypeAll(typeof(Light)) as Light[]) {
        if (CVRHooks.LocalAvatar == null || !light.transform.IsChildOf(CVRHooks.LocalAvatar.transform)) {
          MatchDPSLight(light, SensorOwnerType.World);
        }
      }
      foreach (var gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[]) {
        if (CVRHooks.LocalAvatar == null || !gameObject.transform.IsChildOf(CVRHooks.LocalAvatar.transform)) {
          MatchThrustVector(gameObject, SensorOwnerType.World);
        }
      }
    }

    private void OnAvatarIsReady(object sender, CVRHooks.AvatarEventArgs args)
    {
      foreach (var light in args.Avatar.GetComponentsInChildren<Light>(true)) {
        MatchDPSLight(light, args.Player ? SensorOwnerType.RemotePlayer : SensorOwnerType.LocalPlayer);
      }
      foreach (var gameObject in args.Avatar.GetComponentsInChildren<GameObject>(true)) {
        MatchThrustVector(gameObject, args.Player ? SensorOwnerType.RemotePlayer : SensorOwnerType.LocalPlayer);
      }
    }

    private void OnPropIsReady(object sender, GameObject obj)
    {
      foreach (var light in obj.GetComponentsInChildren<Light>(true)) {
        MatchDPSLight(light, SensorOwnerType.World);
      }
      foreach (var gameObject in obj.GetComponentsInChildren<GameObject>(true)) {
        MatchThrustVector(gameObject, SensorOwnerType.World);
      }
    }

    private void MatchDPSLight(Light light, SensorOwnerType sensorType)
    {
      if (DPSLightId(light, DPSPenetrator.TipID) || DPSLightId(light, DPSPenetrator.TipID_ZawooCompat)) {
        // Step up the hierarchy until we find an object with a mesh as child
        GameObject root;
        for (root = light.gameObject; root != null && root.GetComponentInChildren<MeshRenderer>(true) == null && root.GetComponentInChildren<SkinnedMeshRenderer>(true) == null; root = root.transform.parent?.gameObject);
        if (root == null) {
          Error("Discovered a misconfigured Dynamic Penetration System penetrator. Penetrators must have a mesh somewhere in its hierarchy!");
          return;
        }

        GameObject meshObject;
        Mesh mesh;
        var meshRenderer = root.GetComponentInChildren<MeshRenderer>(true);
        if (meshRenderer) {
          meshObject = meshRenderer.gameObject;
          mesh = meshRenderer.GetComponent<MeshFilter>()?.sharedMesh;
        } else {
          var skinnedMeshRenderer = root.GetComponentInChildren<SkinnedMeshRenderer>(true);
          meshObject = skinnedMeshRenderer?.gameObject;
          mesh = skinnedMeshRenderer?.sharedMesh;
        }
        if (mesh == null) {
          Error("Misconfigured Dynamic Penetration System penetrator; Couldn't find mesh");
          return;
        }

        var penetrator = new DPSPenetrator(root.name, sensorType, root, meshObject, mesh, light);
        mGivers[root.GetInstanceID()] = penetrator;
        mSensorInstances[root.GetInstanceID()] = penetrator;
        SensorDiscovered?.Invoke(this, penetrator);
      } else if (DPSLightId(light, DPSOrifice.MiddleID) || DPSLightId(light, DPSOrifice.EntranceID_A) || DPSLightId(light, DPSOrifice.EntranceID_B)) {
        var gameObject = light.gameObject.transform.parent.gameObject;
        DPSOrifice orifice;
        if (!mTakers.ContainsKey(gameObject.GetInstanceID())) {
          orifice = new DPSOrifice(gameObject.name, sensorType, gameObject);
        } else {
          orifice = mTakers[gameObject.GetInstanceID()] as DPSOrifice;
        }
        if (DPSLightId(light, DPSOrifice.MiddleID)) {
          orifice.mLight1 = light;
        } else if (DPSLightId(light, DPSOrifice.EntranceID_A) || DPSLightId(light, DPSOrifice.EntranceID_B)) {
          orifice.mLight2 = light;
        }
        mTakers[gameObject.GetInstanceID()] = orifice;
        mSensorInstances[gameObject.GetInstanceID()] = orifice;
        if (orifice.mLight1 != null && orifice.mLight2 != null) {
          SensorDiscovered?.Invoke(this, orifice);
        }
      }
    }

    private static bool DPSLightId(Light light, float id)
    {
      return light.color.maxColorComponent < 0.01f && Mathf.Abs((light.range % 0.1f) - id) < 0.001f;
    }

    private void MatchThrustVector(GameObject gameObject, SensorOwnerType sensorType)
    {
      if (!Giver.Pattern.Match(gameObject.name).Success || !Taker.Pattern.Match(gameObject.name).Success) return;

      // Don't overwrite DPS sensors if user does manual configuration
      if (mSensorInstances.ContainsKey(gameObject.GetInstanceID())) {
        Util.DebugLog($"Skipping ThustVector \"{gameObject.name}\" since it was already claimed by DPS");
        return;
      }

      // If object contains mesh, treat it as a penetrator, otherwise orifice
      GameObject meshObject;
      Mesh mesh;
      var meshRenderer = gameObject.GetComponentInChildren<MeshRenderer>(true);
      if (meshRenderer) {
        meshObject = meshRenderer.gameObject;
        mesh = meshRenderer.GetComponent<MeshFilter>()?.sharedMesh;
      } else {
        var skinnedMeshRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer>(true);
        meshObject = skinnedMeshRenderer?.gameObject;
        mesh = skinnedMeshRenderer?.sharedMesh;
      }

      if (mesh != null) {
        var penetrator = new Giver(gameObject.name, sensorType, gameObject, meshObject, mesh);
        mGivers[gameObject.GetInstanceID()] = penetrator;
        mSensorInstances[gameObject.GetInstanceID()] = penetrator;
        SensorDiscovered?.Invoke(this, penetrator);
      } else {
        var orifice = new Taker(gameObject.name, sensorType, gameObject);
        mTakers[gameObject.GetInstanceID()] = orifice;
        mSensorInstances[gameObject.GetInstanceID()] = orifice;
        SensorDiscovered?.Invoke(this, orifice);
      }
    }

    public void OnUpdate()
    {
      // Check for destructions
      var lostSensors = new List<int>();
      foreach (var kv in mSensorInstances) {
        if (kv.Value.GameObject == null) {
          lostSensors.Add(kv.Key);
        }
      }
      foreach (var id in lostSensors) {
        var lostSensor = mSensorInstances[id];
        mSensorInstances.Remove(id);
        mGivers.Remove(id);
        mTakers.Remove(id);
        SensorLost?.Invoke(this, lostSensor);
      }

      // Zero sensor values
      foreach (var penetrator in mGivers.Values) {
        penetrator.mValue = 0f;
      }
      foreach (var orifice in mTakers.Values) {
        orifice.mCumulativeValue = 0f;
        orifice.mNumPenetrators = 0;
      }

      // Update penetrations
      foreach (var giver in mGivers.Values) {
        if (!giver.Active) continue;

        var p0 = giver.mMeshObject.transform.position;

        foreach (var taker in mTakers.Values) {
          if (!taker.Active) continue;
          if (!giver.Enabled && !taker.Enabled) continue;

          // Simple euclidian distance
          // Dynamic Penetration System doesn't take the increased distance of the bezier into account, it simply stretches the penetrator to fit along it, so this is actually fine!
          var p1 = taker.GameObject.transform.position;
          // TODO: Angle limit?
          float distance = Vector3.Distance(p0, p1);
          float depth = Math.Max(0f, Math.Min(1 - distance / giver.Length, 1f));
          if (depth > giver.mValue) {
            // Util.DebugLog($"{giver.Name}: 1 - {distance} / {giver.Length} = {depth}");
            giver.mValue = depth;
            taker.mCumulativeValue += depth;
            taker.mNumPenetrators += 1;
          }
        }
      }
    }
  }
}