using System;
using System.Reflection;
using System.Collections.Generic;
using ABI_RC.Core.EventSystem;
using ABI_RC.Core.IO;
using ABI_RC.Core.Player;
using ABI_RC.Core.Savior;
using HarmonyLib;
using UnityEngine;

namespace VibeGoesBrrr
{
  static class CVRHooks
  {
    public class AvatarEventArgs : EventArgs
    {
      public GameObject Avatar;
      public PlayerDescriptor Player;
    }

    public static event EventHandler<AvatarEventArgs> AvatarIsReady;
    public static event EventHandler<GameObject> PropIsReady;

    public static GameObject LocalAvatar = null;

    private static HashSet<string> PendingProps = new HashSet<string>();

    public static void OnApplicationStart(HarmonyLib.Harmony harmony)
    {
      harmony.Patch(typeof(PlayerSetup).GetMethod(nameof(PlayerSetup.SetupAvatar), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(OnLocalAvatarLoad), BindingFlags.NonPublic | BindingFlags.Static)));
      harmony.Patch(typeof(PuppetMaster).GetMethod(nameof(PuppetMaster.AvatarInstantiated), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(OnRemoteAvatarLoad), BindingFlags.NonPublic | BindingFlags.Static)));
      harmony.Patch(typeof(CVRObjectLoader).GetMethod(nameof(CVRObjectLoader.InstantiateProp), BindingFlags.Public | BindingFlags.Instance),
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(InstantiateProp), BindingFlags.NonPublic | BindingFlags.Static)));
      harmony.Patch(typeof(GameObject).GetMethod(nameof(GameObject.SetActive), BindingFlags.Public | BindingFlags.Instance),
        postfix: new HarmonyMethod(typeof(CVRHooks).GetMethod(nameof(SetActive), BindingFlags.NonPublic | BindingFlags.Static)));
    }

    private static void OnLocalAvatarLoad(GameObject __0)
    {
      Util.DebugLog("OnLocalAvatarLoad");
      LocalAvatar = __0;
      AvatarIsReady?.Invoke(null, new AvatarEventArgs { Avatar = __0, Player = null });
    }

    private static void OnRemoteAvatarLoad(PuppetMaster __instance)
    {
      var playerDescriptor = Traverse.Create(__instance).Field("_playerDescriptor").GetValue<PlayerDescriptor>();
      Util.DebugLog($"OnRemoteAvatarLoad: {playerDescriptor.userName}");
      AvatarIsReady?.Invoke(null, new AvatarEventArgs { Avatar = __instance.avatarObject, Player = playerDescriptor });
    }

    private static void InstantiateProp(DownloadTask.ObjectType t, AssetManagement.PropTags tags, string objectId, string instTarget, byte[] b)
    {
      if (t == DownloadTask.ObjectType.Prop)
      {
        PendingProps.Add(instTarget);
      }
    }
    private static void SetActive(GameObject __instance, bool value)
    {
      if (value == true && PendingProps.Count > 0 && PendingProps.Remove(__instance.name))
      {
        PropIsReady?.Invoke(null, __instance);
      }
    }
    public static void Vibrate(float delay = 0.0f, float duration = 0.0f, float frequency = 440f, float amplitude = 1f, bool hand = false)
    {
      CVRInputManager.Instance.Vibrate(delay, duration, frequency, amplitude, hand);
    }
  }
}